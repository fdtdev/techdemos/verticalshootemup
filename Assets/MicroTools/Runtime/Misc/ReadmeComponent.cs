﻿using UnityEngine;

namespace com.FDT.MicroTools.Misc
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     Quick and fast component i did to be able to better visually document this project
    /// Changelog:       
    /// </summary>
    public class ReadmeComponent : MonoBehaviour
    {
        [TextArea(3, 25), SerializeField] private string _description;
    }

}
