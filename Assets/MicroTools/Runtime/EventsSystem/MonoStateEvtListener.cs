﻿using UnityEngine;
using UnityEngine.Events;

namespace com.FDT.MicroTools.Events
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     
    /// Changelog:       
    /// </summary>
    public class MonoStateEvtListener : MonoBehaviour
    {
        [SerializeField] protected MonoStateEvt _evt;
        [SerializeField] protected UnityEvent _uEvt;
        private void OnEnable()
        {
            _evt.AddListener(HandleEvt);
        }

        private void HandleEvt()
        {
            _uEvt.Invoke();
        }

        private void OnDisable()
        {
            _evt.RemoveListener(HandleEvt);
        }
    }
}