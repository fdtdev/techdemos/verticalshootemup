﻿using System;
using System.Collections.Generic;
using com.FDT.Common.ReloadedScriptableObject;
using UnityEngine;

namespace com.FDT.MicroTools.Events
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     
    /// Changelog:       
    /// </summary>
    [CreateAssetMenu(menuName = "VerticalShootEmUp/Events/MonoStateEvt", fileName = "MonoStateEvt")]
    public class MonoStateEvt : ScriptableObject, IMonoState
    {
        [SerializeField] protected bool _debug;
        [SerializeField] protected string _id;
        public string Id => _id;
        private static Dictionary<string, Action> listeners = new Dictionary<string,Action>();
        
        public void ResetValues()
        {
            listeners = new Dictionary<string,Action>();
        }

        public void AddListener(Action callback)
        {
            if (_debug)
            {
                Debug.Log($"<color=yellow>AddListener</color> <color=cyan>{_id}</color>", this);    
            }
            if (!listeners.ContainsKey(_id))
                listeners.Add(_id, callback);
            else
                listeners[name] += callback;
        }

        public void RemoveListener(Action callback)
        {
            if (_debug)
            {
                Debug.Log($"<color=green>RemoveListener</color> <color=cyan>{_id}</color>", this);    
            }
            listeners[_id] -= callback;
        }

        public void TriggerEvent()
        {
            if (_debug)
            {
                Debug.Log($"<color=red>TriggerEvent</color> <color=cyan>{_id}</color>", this);    
            }
            if (listeners.ContainsKey(_id))
            {
                listeners[_id].Invoke();
            }
        }
    }
}
