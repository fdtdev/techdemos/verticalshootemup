﻿using System;
using System.Collections.Generic;
using com.FDT.Common.ReloadedScriptableObject;
using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp.Events
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     
    /// Changelog:       
    /// </summary>
    public abstract class MonoStateEvT2<T0, T1> : ScriptableObject, IMonoState
    {
        [SerializeField] protected bool _debug;
        [SerializeField] protected string _id;
        public string Id => _id;
        private static Dictionary<string, Action<T0, T1>> listeners = new Dictionary<string,Action<T0, T1>>();
        
        public void ResetValues()
        {
            listeners = new Dictionary<string,Action<T0, T1>>();
        }

        public void AddListener(Action<T0, T1> callback)
        {
            if (_debug)
            {
                Debug.Log($"<color=yellow>AddListener</color> <color=cyan>{_id}</color>", this);    
            }
            if (!listeners.ContainsKey(_id))
                listeners.Add(_id, callback);
            else
                listeners[name] += callback;
        }

        public void RemoveListener(Action<T0, T1> callback)
        {
            if (_debug)
            {
                Debug.Log($"<color=green>RemoveListener</color> <color=cyan>{_id}</color>", this);    
            }
            listeners[_id] -= callback;
        }

        public void TriggerEvent(T0 arg0, T1 arg1)
        {
            if (_debug)
            {
                Debug.Log($"<color=red>TriggerEvent</color> <color=cyan>{_id}</color> with arg0 {arg0} , arg1 {arg1}", this);    
            }
            if (listeners.ContainsKey(_id))
            {
                listeners[_id].Invoke(arg0, arg1);
            }
        }
    }
}