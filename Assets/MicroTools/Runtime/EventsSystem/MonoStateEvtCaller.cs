﻿using UnityEngine;

namespace com.FDT.MicroTools.Events
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     
    /// Changelog:       
    /// </summary>
    public class MonoStateEvtCaller : MonoBehaviour
    {
        [SerializeField] protected bool _runOnEnable = false;
        [SerializeField] protected MonoStateEvt _evt;

        private void OnEnable()
        {
            if (_runOnEnable)
                TriggerEvent();
            
        }

        public void TriggerEvent()
        {
            _evt.TriggerEvent();
        }
    }
}