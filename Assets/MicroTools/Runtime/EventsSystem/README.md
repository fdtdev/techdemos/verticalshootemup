Event System:

This is a very tiny version of my GameEvents system, reduced in complexity in exchange for a less ideal handling of edge cases.
The GameEvent system also has a complete Property Drawer support that gives the active listeners in realtime while in runtime, something that this version lacks.

But because the GameEvent system had some changes introduced by other coders, it would be not useful to evaluate how I work. Also, it's complexity could make it hard to understand as it is designed to be used as a black box.

Description:
The system has a no parameter version, and it has several multi-parameter generic implementations that, as today, have to be extended manually because of the lack of generics supports in the Unity serializer.
Also, each event type could have 2 support components. The Trigger is a component that can be called from outside to trigger an event, and the listener, that listens to a MonoState event and triggers a compatible UnityEvent.
