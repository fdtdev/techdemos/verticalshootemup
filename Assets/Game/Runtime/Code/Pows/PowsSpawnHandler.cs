﻿using System.Collections.Generic;
using com.FDT.MicroTools.Misc;
using com.NewMoon.VerticalShootEmUp.GameActors;
using com.NewMoon.VerticalShootEmUp.Pool;
using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp.Pows
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     Detects and filters collision, to spawn a random power
    ///                  up every _powTime at minimum.
    /// Changelog:       
    /// </summary>
    public class PowsSpawnHandler : DestroyHandlerBase
    {
        [System.Serializable]
        public class PowChanceData
        {
            public GameActorIDAsset powId;
            public float chance = 1;
        }
        [SerializeField] protected float _powTime = 5f;
        [SerializeField] protected GameActorMultiPoolHandler _multiPool;
        [SerializeField] protected List<PowChanceData> _powChanceData = new List<PowChanceData>();
        private readonly Dictionary<GameActorIDAsset, float> powerUpsChanceDictionary = new Dictionary<GameActorIDAsset, float>();

        private float lastPowTime = 0;
        protected override void HandleOnEnable()
        {
            powerUpsChanceDictionary.Clear();
            for (int i = 0; i < _powChanceData.Count; i++)
            {
                powerUpsChanceDictionary.Add(_powChanceData[i].powId, _powChanceData[i].chance);
            }
        }

        protected override void HandleExplosion(GameActor obj)
        {
            if (Time.time >= lastPowTime + _powTime)
            {
                var powId = powerUpsChanceDictionary.RandomElementByWeight(e => e.Value).Key;
                if (powId != null)
                {
                    lastPowTime = Time.time;
                    SpawnPow(powId, obj);
                }
            }
        }

        private void SpawnPow(GameActorIDAsset powId, GameActor obj)
        {
            var pool = _multiPool.GetPool(powId);
            var o = pool.Get(false);
            o.transform.position = obj.transform.position;
            o.gameObject.SetActive(true);
        }
    }
}