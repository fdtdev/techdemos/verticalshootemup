﻿
namespace com.NewMoon.VerticalShootEmUp.Levels
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     
    /// Changelog:       
    /// </summary>
    public enum LevelStepType
    {
        WAVE = 0, END_LEVEL = 1, SHOW_TEXT = 2
    }
}