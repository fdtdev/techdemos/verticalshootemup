﻿namespace com.NewMoon.VerticalShootEmUp.Levels
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     Different game modes
    /// Changelog:       
    /// </summary>
    public enum GameMode
    {
        ARCADE = 0,
        TIME = 1
    }
}
