﻿using com.NewMoon.VerticalShootEmUp.GameActors;
using com.NewMoon.VerticalShootEmUp.Pool;

namespace com.NewMoon.VerticalShootEmUp.Levels
{
    [System.Serializable]
    public class SpawnerData
    {
        public GameActorIDAsset id;
        public GameActorPoolFactory pool;
    }
}