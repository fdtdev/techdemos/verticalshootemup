﻿using com.NewMoon.VerticalShootEmUp.GameActors;
using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp.GameActorFeatures
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     ScriptableObject implementation of IGameActorPhysics, just to show the power of the
    ///                  interface based adapter pattern.
    /// Changelog:       
    /// </summary>
    [CreateAssetMenu(menuName = "VerticalShootEmUp/GameActorPhysicsAsset", fileName = "GameActorPhysicsAsset")]
    public class GameActorPhysicsAsset : ScriptableObject, IGameActorPhysics
    {
        [SerializeField] protected float _acceleration;
        public float Acceleration => _acceleration;
    }
}