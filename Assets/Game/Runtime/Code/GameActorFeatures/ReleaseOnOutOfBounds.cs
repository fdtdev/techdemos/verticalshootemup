﻿using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp.GameActorFeatures
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     This components registers statically the bounds of the game to let the gameActors know
    ///                  when it's crossed to be released.
    /// Changelog:       
    /// </summary>
    public class ReleaseOnOutOfBounds : GameActorFeatureBase
    {

        private void OnCollisionExit2D(Collision2D other)
        {
            if (other.gameObject == GameplayBounds.Gameplay.gameObject)
            {
                _gameActor.SetReleased();
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.gameObject == GameplayBounds.Gameplay.gameObject)
            {
                _gameActor.SetReleased();
            }
        }
    }
}