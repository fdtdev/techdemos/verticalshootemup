﻿using com.NewMoon.VerticalShootEmUp.GameActorFeatures;
using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp.Collisions
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     This registers the collider for the gameActor, and triggers collisions. This needs
    ///                  to be in the same place as the colliders because it uses the Unity callbacks. I could make
    ///                  them be like the other features and use collisions manually, but it was added complexity
    ///                  not needed for the scope of this project.
    /// Changelog:       
    /// </summary>
    public class CollisionsHandler : GameActorFeatureBase
    {
        [SerializeField] protected Rigidbody2D _rb2d;
        
        private void OnEnable()
        {
            CollidersRegister.Register(_gameActor, _rb2d);
        }

        private void OnDisable()
        {
            CollidersRegister.Unregister(_rb2d);
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.rigidbody == null) return;
            
            var otherActor = CollidersRegister.Get(other.rigidbody);
            CollidersRegister.TriggerCollision(_gameActor, otherActor, other.gameObject);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.attachedRigidbody == null) return;
            
            var otherActor = CollidersRegister.Get(other.attachedRigidbody);
            CollidersRegister.TriggerCollision(_gameActor, otherActor, other.gameObject);
        }
    }
}