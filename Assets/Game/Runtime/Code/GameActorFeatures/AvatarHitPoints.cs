﻿using com.FDT.MicroTools.Events;
using com.NewMoon.VerticalShootEmUp.Levels;
using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp.GameActorFeatures
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     Damage received and inflicted specifically for the avatar. This saves the data in
    ///                  CurrentLevelDataAsset monoState.
    /// Changelog:       
    /// </summary>
    public class AvatarHitPoints : HitPointsBase
    {
        [SerializeField] protected MonoStateEvt _avatarHealthChangedEvt;
        [SerializeField] protected CurrentLevelDataAsset _currentLevelData;
        [SerializeField] protected int _baseHealth;
        protected override int BaseHealth => _baseHealth;

        protected override int Health
        {
            get => _currentLevelData.AvatarHealth;
            set => _currentLevelData.SetAvatarHealth(value);
        }

        protected override void HandleHealthChanged()
        {
            if (Health == 0)
            {
                int lives = _currentLevelData.CurrentLives;
                _currentLevelData.SetCurrentLives(--lives);
            }
            _avatarHealthChangedEvt.TriggerEvent();
        }


    }
}
