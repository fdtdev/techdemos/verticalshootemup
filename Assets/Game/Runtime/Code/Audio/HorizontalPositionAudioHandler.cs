﻿using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp.Audio
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     Different game modes
    /// Changelog:       Simple sound player that uses a fixed pool of audioSources.
    /// </summary>
    public class HorizontalPositionAudioHandler : AudioHandler
    {
        [SerializeField] protected Camera _mainCamera;
        public void PlaySound(string sndId, Vector3 pos)
        {
            SoundData sndData = GetSoundData(sndId);
            if (sndData != null)
            {
                AudioSource aSource = null;
                if (sndData.spamProtection != SpamProtectionType.NONE)
                {
                    aSource = GetFirstPlaying(sndData);
                    if (sndData.spamProtection == SpamProtectionType.AVOID) 
                    {
                        return;
                    }
                }
                if (aSource == null)
                {
                    aSource = GetFreeAudioSource();    
                }
                if (aSource != null)
                {
                    var screenPos = _mainCamera.WorldToViewportPoint(pos);
                    float screenHorizontalPos = (screenPos.x / (1)) * (2) + (-1);
                    
                    aSource.panStereo = screenHorizontalPos;
                    aSource.clip = sndData.clip;
                    aSource.PlayOneShot(sndData.clip);
                }
                else
                {
                    Debug.LogWarning($"no available channel to play sound id {sndId}");
                }
            }
            else
            {
                Debug.LogError($"sound id {sndId} not found");
            }
        }
    }
}