﻿using System.Collections.Generic;
using com.NewMoon.VerticalShootEmUp.GameActors;
using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp.Audio
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     
    /// Changelog:       
    /// </summary>
    public class HorizontalAudioGameActorListener : MonoBehaviour
    {
        [System.Serializable]
        public class AudioGameActor
        {
            public GameActorIDAsset gameActorId;
            public string soundId;
        }

        [SerializeField] protected List<AudioGameActor> _audioGameActorData = new List<AudioGameActor>();
        [SerializeField] protected HorizontalPositionAudioHandler _audioHandler;
        private readonly Dictionary<GameActorIDAsset, string> soundIdByGameActorId = new Dictionary<GameActorIDAsset, string>();

        private void OnEnable()
        {
            soundIdByGameActorId.Clear();
            for (int i = 0; i < _audioGameActorData.Count; i++)
            {
                soundIdByGameActorId.Add(_audioGameActorData[i].gameActorId, _audioGameActorData[i].soundId);
            }
        }

        public void OnReceiveEvent(GameActor g)
        {
            if (soundIdByGameActorId.ContainsKey(g.Id))
            {
                _audioHandler.PlaySound(soundIdByGameActorId[g.Id], g.transform.position);    
            }
        }
    }
}