﻿using System.Collections.Generic;
using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp.Audio
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     Different game modes
    /// Changelog:       Simple sound player that uses a fixed pool of audioSources.
    /// </summary>
    public class AudioHandler : MonoBehaviour
    {
        public enum SpamProtectionType
        {
            NONE = 0, OVERRIDE = 1, AVOID = 2
        }
        [System.Serializable]
        public class SoundData
        {
            public string sndId;
            public AudioClip clip;
            public SpamProtectionType spamProtection = SpamProtectionType.NONE;
        }

        [SerializeField] protected SoundData[] _soundDatas;
        [SerializeField] protected List<AudioSource> _audioSources = new List<AudioSource>();

        /// <summary>
        /// Plays a sound by it's sndID, nothing if there's no available AudioSources
        /// </summary>
        /// <param name="sndId"></param>
        public void PlaySound(string sndId)
        {
            SoundData sndData = GetSoundData(sndId);
            if (sndData != null)
            {
                AudioSource aSource = null;
                if (sndData.spamProtection != SpamProtectionType.NONE)
                {
                    aSource = GetFirstPlaying(sndData);
                    if (sndData.spamProtection == SpamProtectionType.AVOID) 
                    {
                        return;
                    }
                }
                if (aSource == null)
                {
                    aSource = GetFreeAudioSource();    
                }
                if (aSource != null)
                {
                    aSource.clip = sndData.clip;
                    aSource.PlayOneShot(sndData.clip);    
                }
                else
                {
                    Debug.LogWarning($"no available channel to play sound id {sndId}");
                }
            }
            else
            {
                Debug.LogError($"sound id {sndId} not found");
            }
        }

        protected AudioSource GetFirstPlaying(SoundData sndData)
        {
            for (int i = 0; i < _audioSources.Count; i++)
            {
                if (_audioSources[i].isPlaying && _audioSources[i].clip == sndData.clip)
                    return _audioSources[i];
            }
            return null;
        }

        protected AudioSource GetFreeAudioSource()
        {
            for (int i = 0; i < _audioSources.Count; i++)
            {
                if (!_audioSources[i].isPlaying)
                    return _audioSources[i];
            }
            return null;
        }

        protected SoundData GetSoundData(string sndId)
        {
            foreach (var s in _soundDatas)
            {
                if (s.sndId == sndId)
                    return s;
            }
            return null;
        }
    }
}

