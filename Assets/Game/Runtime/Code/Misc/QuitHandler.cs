﻿using com.FDT.Common;
using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp.Misc
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     Quit request handler.
    /// Changelog:       
    /// </summary>
    public class QuitHandler : MonoBehaviour
    {
        public void RequestQuit()
        {
            RuntimeApplication.QuitGame();
        }
    }
}