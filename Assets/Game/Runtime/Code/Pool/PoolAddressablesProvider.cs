using System.Threading;
using System.Threading.Tasks;
using com.FDT.Pools;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace com.NewMoon.VerticalShootEmUp.Pool
{
    /// <summary>
    /// Creation Date:   2/27/2021 12:07:55 AM
    /// Product Name:    FDTModules
    /// Developers:      FDT
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [System.Serializable]
    public class PoolAddressablesProvider : IPoolAssetProvider
    {
        [SerializeField] protected AssetReferenceGameObject _prefabReference;
        private AsyncOperationHandle<GameObject> prefabHandle;
        
        public async Task<GameObject> GetAsset(CancellationToken t)
        {
            prefabHandle = _prefabReference.LoadAssetAsync<GameObject>();
            
            do
            {
                await Task.Yield();
            } while (!prefabHandle.IsDone && !t.IsCancellationRequested);
            
            if (t.IsCancellationRequested)
                return null;
            return prefabHandle.Result;
        }

        public void OnDestroy()
        {
            if (prefabHandle.IsValid() && prefabHandle.Status != AsyncOperationStatus.None)
            {
                Addressables.Release(prefabHandle);
            }
        }
    }
}