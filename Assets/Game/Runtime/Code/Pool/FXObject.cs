﻿using System.Collections;
using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp.Pool
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     This is used to pool FX objects
    /// Changelog:       
    /// </summary>
    public class FXObject : BasePooleable
    {
        [SerializeField] protected float _duration;
        private Coroutine c;

        protected override void DoOnEnable()
        {
            base.DoOnEnable();
            c = StartCoroutine(DoWaitAndRelease());
        }

        IEnumerator DoWaitAndRelease()
        {
            yield return new WaitForSeconds(_duration);
            c = null;
            SetReleased();
        }

        protected override void DoOnDisable()
        {
            base.DoOnDisable();
            if (c != null)
            {
                StopCoroutine(c);
                c = null;
            }
        }
    }
}