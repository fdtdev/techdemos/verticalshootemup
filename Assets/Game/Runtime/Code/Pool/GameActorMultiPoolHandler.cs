﻿using System.Collections.Generic;
using com.FDT.Common;
using com.FDT.Pools;
using com.NewMoon.VerticalShootEmUp.GameActors;
using UnityEngine;
using UnityEngine.Serialization;

namespace com.NewMoon.VerticalShootEmUp.Pool
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     This system handles multiple GameActor pools to make them initialize together. Also
    ///                  it provides a pool according to the provided GameActorIDAsset. This class is useful to
    ///                  initialize important pools and be sure that they will be available.
    /// Changelog:       
    /// </summary>
    public class GameActorMultiPoolHandler : MonoBehaviour
    {
        [FormerlySerializedAs("_gameActorPools")] [SerializeField, ObjectType(typeof(IPool))] protected List<Object> _pools = new List<Object>();
        public IPool GetPool(GameActorIDAsset gameActorId)
        {
            foreach (var pool in _pools)
            {
                if (pool is GameActorPoolFactory gaPool)
                {
                    if (gaPool.gameActorID == gameActorId)
                    {
                        return gaPool;
                    }
                }
            }
            return null;
        }
    }
}