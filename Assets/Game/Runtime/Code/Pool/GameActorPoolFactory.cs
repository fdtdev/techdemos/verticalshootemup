﻿using ccom.NewMoon.VerticalShootEmUp.Pool;
using com.NewMoon.VerticalShootEmUp.GameActors;
using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp.Pool
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     GameActor pool implementation. Almost all the pools in the game are of this type.
    /// Changelog:       
    /// </summary>
    public class GameActorPoolFactory : InitPoolFactory<GameActor, PoolAddressablesProvider>
    {
        [SerializeField] protected GameActorIDAsset _gameActorID;
        public GameActorIDAsset gameActorID => _gameActorID;
    }
}