﻿using com.FDT.Pools;
using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp.Pool
{
    public class BasePooleable : MonoBehaviour, IPooleable
    {
        private bool toDestroy = false;

        private void OnEnable()
        {
            toDestroy = false;
            DoOnEnable();
        }

        private void OnDisable()
        {
            DoOnDisable();
        }
        protected virtual void DoOnEnable()
        {
            
        }
        private bool CheckDestroy()
        {
            if (toDestroy)
            {
                poolData.ReturnToPool();
                return true;
            }

            return false;
        }
        
        // Update is called once per frame
        private void Update()
        {
            if (CheckDestroy())
            {
                return;
            }
            DoUpdate();
        }

        private void FixedUpdate()
        {
            if (CheckDestroy())
            {
                return;
            }
            DoFixedUpdate();
        }
        public void SetReleased()
        {
            toDestroy = true;
        }
        public void SetDestroyed()
        {
            DoOnDestroyed();
            SetReleased();
        }
        protected virtual void DoUpdate()
        {
            
        }
        protected virtual void DoFixedUpdate()
        {
            
        }
        protected virtual void DoOnDestroyed()
        {

        }

        protected virtual void DoOnDisable()
        {
            
        }

        protected PoolData _poolData = new PoolData();
        public PoolData poolData
        {
            get => _poolData; }
    }
}