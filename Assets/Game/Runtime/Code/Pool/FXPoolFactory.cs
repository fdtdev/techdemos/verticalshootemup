﻿using ccom.NewMoon.VerticalShootEmUp.Pool;

namespace com.NewMoon.VerticalShootEmUp.Pool
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     FXObject pool implementation. Used by effects.
    /// Changelog:       
    /// </summary>
    public class FXPoolFactory : InitPoolFactory<FXObject, PoolAddressablesProvider>
    {
    }
}