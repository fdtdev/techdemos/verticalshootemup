﻿using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp.UI
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     This manages the pause functionality. It's expected to only have pauses in the game when a
    ///                  popUp is present.
    /// Changelog:       
    /// </summary>
    public class PopUp : MonoBehaviour
    {
        private static int pausedLock;

        private void OnEnable()
        {
            pausedLock++;
            RefreshPause();
        }

        private void RefreshPause()
        {
            Time.timeScale = pausedLock > 0 ? 0 : 1;
        }

        private void OnDisable()
        {
            pausedLock--;
            RefreshPause();
        }
    }
}