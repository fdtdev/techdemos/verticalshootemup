﻿using com.FDT.MicroTools.Misc;
using com.NewMoon.VerticalShootEmUp.Events.Custom;
using com.NewMoon.VerticalShootEmUp.GameActorFeatures;
using com.NewMoon.VerticalShootEmUp.GameActors;
using com.NewMoon.VerticalShootEmUp.Pool;
using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp.Weapons
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     Receives the request event for the shoot, understands the content of the WeaponDataAsset and
    ///                  spawn the bullets according to it.
    /// Changelog:       
    /// </summary>
    public class WeaponsHandler : GameActorMultiPoolHandler
    {
        [Header("Events"), SerializeField] protected RequestShootEvt _requestShootEvt;
        [SerializeField] protected GameActorEvt _onShootBulletEvt;
        private bool canShoot = false;

        private void OnEnable()
        {
            _requestShootEvt.AddListener(HandleShootRequest);
        }
        private void OnDisable()
        {
            _requestShootEvt.RemoveListener(HandleShootRequest);
        }

        public void HandleInit()
        {
            canShoot = true;
        }

        private void HandleShootRequest(GameActor arg1, WeaponData arg2)
        {
            for (int i = 0; i < arg2.bulletDatas.Count; i++)
            {
                SpawnBullet(arg1.transform.position, arg2.bulletDatas[i]);
            }
        }
        private void SpawnBullet(Vector3 pos, BulletData bdata)
        {
            if (!canShoot) return;
            
            var pool = GetPool(bdata.bullet);
            var o = pool.Get(false);
            o.transform.position = pos + (Vector3)bdata.offset;
            o.gameObject.SetActive(true);
            if (o is GameActor actor)
            {
                if (bdata.setMovement)
                {
                    if (actor.GetFeature<Movement>(out Movement k))
                    {
                        k.inputProvider.Intention = MathHelper.DegreeToVector2(-bdata.angle + 90);
                    }
                }
                _onShootBulletEvt.TriggerEvent(actor);    
            }
        }
    }
}
