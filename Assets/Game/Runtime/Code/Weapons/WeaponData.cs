﻿using System.Collections.Generic;

namespace com.NewMoon.VerticalShootEmUp.Weapons
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     Part of the data in the WeaponDataAsset. Has the bullets data and the cooldown info.
    /// Changelog:       
    /// </summary>
    [System.Serializable]
    public class WeaponData
    {
        public List<BulletData> bulletDatas;
        public float cooldown;
    }
}
