﻿using System.Collections.Generic;
using com.NewMoon.VerticalShootEmUp.GameActors;
using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp.Weapons
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     This scriptableObject has the data on how the GameActor should fire it's weapons.
    /// Changelog:       
    /// </summary>
    [CreateAssetMenu(menuName = "VerticalShootEmUp/WeaponDataAsset", fileName = "WeaponDataAsset")]
    public class WeaponDataAsset : GameActorIDAsset
    {
        [SerializeField] protected List<WeaponData> _weaponDatas = new List<WeaponData>();
        public int DatasCount => _weaponDatas.Count;

        public float GetCurrentCoolwon(int currentWeaponIdx)
        {
            return _weaponDatas[currentWeaponIdx].cooldown;
        }

        public WeaponData GetCurrent(int currentWeaponIdx)
        {
            return _weaponDatas[currentWeaponIdx];
        }
    }
}