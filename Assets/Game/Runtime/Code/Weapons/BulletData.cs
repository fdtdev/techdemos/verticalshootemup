﻿using com.NewMoon.VerticalShootEmUp.GameActors;
using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp.Weapons
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     Part of the data on WeaponData, used with WeaponDataAsset. setMovement set to true, sets the
    ///                  angle in the bullet after respawn. Else, it is expected to use it's own movement intention.
    /// Changelog:       
    /// </summary>
    [System.Serializable]
    public class BulletData
    {
        public GameActorIDAsset bullet;
        public bool setMovement = true;
        public float angle;
        public Vector2 offset;
    }
}