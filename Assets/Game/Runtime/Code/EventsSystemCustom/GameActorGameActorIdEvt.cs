﻿using com.NewMoon.VerticalShootEmUp.GameActors;
using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp.Events.Custom
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     
    /// Changelog:       
    /// </summary>
    [CreateAssetMenu(menuName = "VerticalShootEmUp/Events/GameActorGameActorIdEvt", fileName = "GameActorGameActorIdEvt")]
    public class GameActorGameActorIdEvt : MonoStateEvT2<GameActor, GameActorIDAsset>
    {

    }
}