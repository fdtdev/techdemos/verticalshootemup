﻿using com.FDT.MicroTools.Events;
using com.NewMoon.VerticalShootEmUp.GameActors;
using UnityEngine.Events;

namespace com.NewMoon.VerticalShootEmUp.Events.Custom
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     
    /// Changelog:       
    /// </summary>
    public class GameActorEvtListener : MonoStateEvt1Listener<GameActor, GameActorEvt, GameActorEvtListener.UEvt>
    {
        [System.Serializable]
        public class UEvt:UnityEvent<GameActor>{}
    }
}