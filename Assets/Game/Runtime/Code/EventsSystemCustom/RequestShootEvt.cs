﻿using com.NewMoon.VerticalShootEmUp.GameActors;
using com.NewMoon.VerticalShootEmUp.Weapons;
using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp.Events.Custom
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     
    /// Changelog:       
    /// </summary>
    [CreateAssetMenu(menuName = "VerticalShootEmUp/Events/RequestShootEvt", fileName = "RequestShootEvt")]
    public class RequestShootEvt : MonoStateEvT2<GameActor, WeaponData>
    {

    }
}