﻿using com.FDT.MicroTools.Events;
using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp.Events.Custom
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     
    /// Changelog:       
    /// </summary>
    [CreateAssetMenu(menuName = "VerticalShootEmUp/Events/StringMonoStateEvt", fileName = "StringMonoStateEvt")]
    public class StringMonoStateEvt : MonoStateEvt1<string>
    {

    }
}