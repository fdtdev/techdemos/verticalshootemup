﻿using com.FDT.MicroTools.Events;
using com.NewMoon.VerticalShootEmUp.GameActors;
using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp.Events.Custom
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     
    /// Changelog:       
    /// </summary>
    [CreateAssetMenu(menuName = "VerticalShootEmUp/Events/GameActorEvt", fileName = "GameActorEvt")]
    public class GameActorEvt : MonoStateEvt1<GameActor>
    {
        
    }
}