﻿using com.FDT.MicroTools.Events;
using UnityEngine.Events;

namespace com.NewMoon.VerticalShootEmUp.Events.Custom
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     
    /// Changelog:       
    /// </summary>
    public class StringMonoStateEvtListener : MonoStateEvt1Listener<string, StringMonoStateEvt, StringMonoStateEvtListener.UEvt>
    {
        [System.Serializable]
        public class UEvt:UnityEvent<string>{}
    }
}