﻿using System.Collections.Generic;
using com.NewMoon.VerticalShootEmUp.Events.Custom;
using com.NewMoon.VerticalShootEmUp.GameActors;
using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     This listens for collision events and filter them according to the list of gameActors.
    /// Changelog:       
    /// </summary>
    public abstract class DestroyHandlerBase : MonoBehaviour
    {
        [SerializeField] protected GameActorEvt _explodedEvt;
        [SerializeField] protected List<GameActorIDAsset> _explodingGameActors = new List<GameActorIDAsset>();

        private void OnEnable()
        {
            _explodedEvt.AddListener(OnExplosion);
            HandleOnEnable();
        }

        protected virtual void HandleOnEnable()
        {
            
        }
        private void OnExplosion(GameActor obj)
        {
            if (!_explodingGameActors.Contains(obj.Id)) return;
            HandleExplosion(obj);
        }

        protected abstract void HandleExplosion(GameActor obj);
        
        // Update is called once per frame
        private void OnDisable()
        {
            _explodedEvt.RemoveListener(OnExplosion);
        }
    }
}