﻿using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     Main static game data structure. This data is meant to be static, so it's
    ///                  not be made to be modified in runtime.
    /// Changelog:       
    /// </summary>
    [CreateAssetMenu(menuName = "VerticalShootEmUp/UserDataAsset", fileName = "UserDataAsset")]
    public class UserDataAsset : ScriptableObject
    {
        private static string savedDataKey = "VerticalShootEmUp_UserData";
        public int highscore = 0;

        public void Load()
        {
            string data = PlayerPrefs.GetString(savedDataKey, null);
            if (string.IsNullOrEmpty(data))
            {
                Save();
            }
            else
            {
                JsonUtility.FromJsonOverwrite(data, this);
            }
        }

        public void Save()
        {
            string data = JsonUtility.ToJson(this);
            PlayerPrefs.SetString(savedDataKey, data);
            PlayerPrefs.Save();
        }
    }
}