﻿using System;
using System.Collections.Generic;
using com.NewMoon.VerticalShootEmUp.GameActors;
using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     Main collider registration system. This provides a cached registry for GameActor's
    ///                  colliders, to retrieve them with the collider as key.
    /// Changelog:       
    /// </summary>
    public static class CollidersRegister
    {
        public static Action<GameActor, GameActor, GameObject> OnCollision;

        private static Dictionary<Rigidbody2D, GameActor> registered = new Dictionary<Rigidbody2D, GameActor>();

        public static void Register(GameActor gameActor, Rigidbody2D rb2d)
        {
            registered.Add(rb2d, gameActor);
        }

        public static void Unregister(Rigidbody2D rb2d)
        {
            registered.Remove(rb2d);
        }

        public static GameActor Get(Rigidbody2D rb)
        {
            if (registered.ContainsKey(rb))
            {
                return registered[rb];
            }

            return null;
        }

        public static void TriggerCollision(GameActor collider, GameActor collided, GameObject collidedGameObject)
        {
            OnCollision?.Invoke(collider, collided, collidedGameObject);
        }
    }
}