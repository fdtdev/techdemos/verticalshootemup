﻿using com.NewMoon.VerticalShootEmUp.GameActors;
using com.NewMoon.VerticalShootEmUp.Pool;
using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp.FX
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     This manages the collision graphical effects by listening to events and
    ///                  filtering according to GameActorIDs to select which collisions spawn effects.
    /// Changelog:       
    /// </summary>
    public class FXHandler : DestroyHandlerBase
    {
        [SerializeField] protected FXPoolFactory _pool;
        protected override void HandleExplosion(GameActor obj)
        {
            var o = _pool.Get(false);
            o.transform.position = obj.transform.position;
            o.gameObject.SetActive(true);
        }
    }
}