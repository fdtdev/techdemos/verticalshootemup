﻿using com.FDT.MicroTools.Events;
using com.NewMoon.VerticalShootEmUp.Events.Custom;
using com.NewMoon.VerticalShootEmUp.GameActors;
using com.NewMoon.VerticalShootEmUp.Levels;
using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp.Score
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     Detects a GameActor destruction and checks if it has score configuration set up in the
    ///                  GameConfigAsset. In that case, adds the score to the one in the CurrentLevelDataAsset instance.
    /// Changelog:       
    /// </summary>
    public class ScoreHandler : MonoBehaviour
    {
        [SerializeField] protected GameConfigAsset _config;
        [SerializeField] protected UserDataAsset _userData;
        [SerializeField] protected CurrentLevelDataAsset _lvlDataAsset;
        [SerializeField] protected GameActorEvt _onGameActorDestroyedEvt;
        [SerializeField] protected MonoStateEvt _refreshScoreEvt;
        private void OnEnable()
        {
            _onGameActorDestroyedEvt.AddListener(HandleActorDestroyed);
        }

        private void HandleActorDestroyed(GameActor obj)
        {
            if (!_config.HasScore(obj.Id)) return;
            
            var sd = _config.GetScoreData(obj.Id);
            _lvlDataAsset.AddScore(sd.score);
            if (_userData.highscore < _lvlDataAsset.Score)
            {
                _userData.highscore = _lvlDataAsset.Score;
            }
            _refreshScoreEvt.TriggerEvent();
        }

        private void OnDisable()
        {
            _onGameActorDestroyedEvt.RemoveListener(HandleActorDestroyed);
        }
    }
}