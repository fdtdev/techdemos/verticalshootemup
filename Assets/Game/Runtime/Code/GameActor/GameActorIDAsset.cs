﻿using com.FDT.Common;
using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp.GameActors
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     GameActor IDAsset. This uses the IDAsset capability to be equal no matter the instance,
    ///                  to be used with AddressableAssets as ID, without having the problems it causes when a
    ///                  ScriptableObject is instantiated by this system.
    /// Changelog:       
    /// </summary>
    [CreateAssetMenu (menuName = "VerticalShootEmUp/GameActorIDAsset", fileName = "GameActorIDAsset")]
    public class GameActorIDAsset : IDAsset
    {
    }
}