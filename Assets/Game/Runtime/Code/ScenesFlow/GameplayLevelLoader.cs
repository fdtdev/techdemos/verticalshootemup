﻿using System.Collections.Generic;
using com.FDT.Sequences;
using com.NewMoon.VerticalShootEmUp.Levels;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.EventSystems;

namespace com.NewMoon.VerticalShootEmUp.ScenesFlow
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     This sets a new level and then it runs a SimpleTimedSequence expected to end up
    ///                  loading the gameplay scene.
    /// Changelog:       
    /// </summary>
    public class GameplayLevelLoader : MonoBehaviour
    {
        [SerializeField] protected List<AssetReference> _levels = new List<AssetReference>();
        [SerializeField] protected CurrentLevelDataAsset _currentLevelData;
        [SerializeField] protected EventSystem _eventSystem;
        [SerializeField] protected SequenceComponent _sequence;

        public void LoadGameplayLevel(int i)
        {
            _eventSystem.enabled = false;
            var a = _levels[i].LoadAssetAsync<LevelAsset>();
            _currentLevelData.LoadLevelHandle = a;
            a.Completed += handle =>
            {
                _currentLevelData.SetLevel(a.Result);
                _sequence.sequence.Run();
            };
        }
    }
}