﻿using System.Collections;
using com.FDT.Common;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace com.NewMoon.VerticalShootEmUp.ScenesFlow
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     Loads an scene by using it's name or a IDAsset with the same name used to avoid errors.
    /// Changelog:       
    /// </summary>
    public class SceneLoader : MonoBehaviour
    {
        private bool loading = false;
        public void LoadScene(string scene)
        {
            if (loading)
            {
                Debug.LogError("FUCK!");
                return;
            }
            Extensions.StartCoroutine(DoLoadScene(scene));
        }

        private IEnumerator DoLoadScene(string scene)
        {
            loading = true;
            var o = SceneManager.LoadSceneAsync("LoadingScene");
            yield return o;
            o = SceneManager.LoadSceneAsync(scene);
            yield return o;
            loading = false;
        }

        public void LoadScene(IDAsset sceneId)
        {
            LoadScene(sceneId.name);
        }
    }
}