﻿using com.FDT.Common.ReloadedScriptableObject;
using com.NewMoon.VerticalShootEmUp.GameActors;
using UnityEngine;

namespace com.NewMoon.VerticalShootEmUp.GameInput
{
    /// <summary>
    /// Product Name:    VerticalShootEmUp
    /// Developers:      Franco Scigliano
    /// Description:     
    /// Changelog:       
    /// </summary>
    [CreateAssetMenu (menuName = "VerticalShootEmUp/PlayerInputAsset", fileName = "PlayerInputAsset")]
    public class PlayerInputAsset : ScriptableObject, IInputProvider, IMonoState
    {
        private static Vector2 _intention;
        private static bool _attack;

        public Vector2 Intention
        {
            get => _intention;
            set
            {
                _intention = value;
                _intention.x = Mathf.Clamp(_intention.x, -1, 1);
                _intention.y = Mathf.Clamp(_intention.y, -1, 1);
            }
        }

        public bool Attack
        {
            get => _attack;
            set => _attack = value;
        }

        public void ResetValues()
        {
            _intention = Vector2.zero;
            _attack = false;
        }
    }
}