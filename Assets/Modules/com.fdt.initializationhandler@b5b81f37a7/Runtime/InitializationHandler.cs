﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using com.FDT.Common;
using UnityEngine;
using UnityEngine.Events;

namespace com.FDT.InitializationHandler
{
    /// <summary>
    /// Creation Date:   26/02/2020 10:54:39
    /// Product Name:    FDT Initialization Handler
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:
    /// Changelog:       
    /// </summary>
    public sealed class InitializationHandler : MonoBehaviour, IInitializable
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        [SerializeField, HideInInspector] private UnityEvent _endEvt;
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        [SerializeField, ObjectType(typeof(IInitWrapper))] private List<Object> _initializationItems = new List<Object>();
        private readonly List<IInitWrapper> _initAddedByCode = new List<IInitWrapper>();
        
        #endregion

        #region Variables
        public bool autorun = false;
        private readonly List<IInitWrapper> _runtimeItems = new List<IInitWrapper>();
        private readonly List<IInitWrapper> _toInitialize = new List<IInitWrapper>();
        private readonly HashSet<IInitWrapper> _initializingItems = new HashSet<IInitWrapper>();
        private readonly HashSet<IInitWrapper> _initializedItems = new HashSet<IInitWrapper>();
        private int _initializeCount = -1;
        private int _count = 0;
        private readonly List<Task> _waitingTasks = new List<Task>();
        private CancellationTokenSource token;
        #endregion
        
        #region Properties, Consts and Statics
        
        #endregion

        #region Public API
        public void Init()
        {
            _toInitialize.Clear();
            _runtimeItems.Clear();
            token = new CancellationTokenSource();
            for (int i = 0; i < _initializationItems.Count; i++)
            {
                if (_initializationItems[i] is IInitWrapper o)
                {
                    o.InitData.InitState = InitState.NOT_INITIALIZED;
                    o.InitData.RuntimeDependences.Clear();
                    _toInitialize.Add(o);
                    _runtimeItems.Add(o);
                }
            }

            if (_initAddedByCode.Count > 0)
            {
                for (int i = 0; i < _initAddedByCode.Count; i++)
                {
                    var o = _initAddedByCode[i];
                    o.InitData.InitState = InitState.NOT_INITIALIZED;
                    o.InitData.RuntimeDependences.Clear();
                    _toInitialize.Add(o);
                    _runtimeItems.Add(o);
                }
            }
             
            _initializingItems.Clear();
            _initializedItems.Clear();
            for (int i = _toInitialize.Count - 1; i >= 0; i--)
            {
                _toInitialize[i].InitData.Init();
            }

            for (int i = _toInitialize.Count-1; i>=0; i--)
            {
                if (!_toInitialize[i].IsEnabled)
                {
                    _toInitialize.RemoveAt(i);
                }
                else
                {
                    _toInitialize[i].InitData.ComputeRuntimeDependences();
                    _toInitialize[i].InitData.InitState = InitState.NOT_INITIALIZED;
                    _toInitialize[i].InitData.AvailableState = AvailableState.AVAILABLE;
                }
            }
            
            _initializeCount = _toInitialize.Count;
            ExecuteItems();
        }
       

        private void FinishInit()
        {
            _endEvt.Invoke();
        }
        #endregion
        #region Methods
     	void Start () {
            if (autorun)
		        Init();
	    }


        private async void ExecuteItems()
        {
            _waitingTasks.Clear();
            do
            {
                List<IInitWrapper> newInits = new List<IInitWrapper>();
                for (int i = 0; i < _toInitialize.Count; i++)
                {
                    bool toInit = true;
                    for(int i2 = 0; i2 < _toInitialize[i].InitData.RuntimeDependences.Count; i2++)
                    {
                        if (_toInitialize[i].InitData.RuntimeDependences[i2].IsEnabled &&
                            !_initializedItems.Contains(_toInitialize[i].InitData.RuntimeDependences[i2]))
                        {
                            toInit = false;
                            break;
                        }
                    }
                    if (toInit)
                    {
                        newInits.Add(_toInitialize[i]);
                    }
                    else
                    {
                        _toInitialize[i].InitData.InitState = InitState.WAITING_DEPENDENCE;
                    }
                }
                _count += newInits.Count;
                for (int i = 0; i < newInits.Count; i ++)
                {
                    _waitingTasks.Add(InitItem(newInits[i]));
                }

                if (!token.IsCancellationRequested)
                {
                    Task finished = await Task.WhenAny(_waitingTasks);
                    _waitingTasks.Remove(finished);
                }

            } while (!Finished && !token.IsCancellationRequested);
            FinishInit();
        }

        private async Task InitItem(IInitWrapper item)
        {
            _toInitialize.Remove(item);
            _initializingItems.Add(item);

            item.InitData.InitState = InitState.INITIALIZING;
            item.Init();

            // cached name to make it available for debug in case the program is shut down
            string itemName = item.ToString();
            
            while (item.InitData.InitState != InitState.INITIALIZED && !token.IsCancellationRequested)
            {
                await Task.Yield();
            }
            if (token.IsCancellationRequested)
            {
                Debug.LogError($"Cancelled initialization of {itemName} due to exit application.");
                return;
            }
            _initializingItems.Remove(item);
            _initializedItems.Add(item);
            _count--;
        }
        private bool Finished =>
            _initializingItems.Count == 0 && _toInitialize.Count == 0 &&
            _initializedItems.Count == _initializeCount;

        public List<IInitWrapper> RuntimeItems
        {
            get => _runtimeItems;
        }

        #endregion

        public void SetUp(List<IInitWrapper> dependences)
        {
            _initAddedByCode.Clear();
            for (int i = 0; i < dependences.Count; i++)
            {
                _initAddedByCode.Add(dependences[i]);
            }
        }
        void OnDestroy () { 
            token?.Cancel ();
        }
    }
}