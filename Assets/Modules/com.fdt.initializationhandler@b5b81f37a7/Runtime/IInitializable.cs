﻿namespace com.FDT.InitializationHandler
{
    /// <summary>
    /// Creation Date:   26/02/2020 10:54:39
    /// Product Name:    FDT Initialization Handler
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:
    /// Changelog:       
    /// </summary>
    public interface IInitializable
    {
        void Init();
    }
}