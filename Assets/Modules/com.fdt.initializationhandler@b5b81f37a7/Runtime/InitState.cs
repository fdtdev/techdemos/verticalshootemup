namespace com.FDT.InitializationHandler
{
    /// <summary>
    /// Creation Date:   8/5/2020 8:19:35 PM
    /// Product Name:    FDT Initialization Handler
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:
    /// Changelog:       
    /// </summary>
    public enum InitState
    {
        NOT_INITIALIZED,
        WAITING_DEPENDENCE,
        INITIALIZING,
        INITIALIZED
    }
}