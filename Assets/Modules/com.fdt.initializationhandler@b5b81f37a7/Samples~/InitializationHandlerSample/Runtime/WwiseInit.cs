using System;
using System.Threading.Tasks;
using UnityEngine;

namespace com.FDT.InitializationHandler.Sample
{
    /// <summary>
    /// Creation Date:   2/21/2021 4:22:24 PM
    /// Product Name:    FDT Initialization Handler
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:
    /// Changelog:         
    /// </summary>
    public class WwiseInit : IInitWrapper
    {
        #region Properties, Consts and Statics
        private InitHandlerData _data = new InitHandlerData();
        public InitHandlerData InitData { get=> _data; }
        public bool IsEnabled { get => true; }
        #endregion
        
        #region Methods
        public async void Init()
        {
            await Task.Delay(TimeSpan.FromSeconds(5));
            _data.InitState = InitState.INITIALIZED;
            Debug.Log($"{this} initialized");
        }
        #endregion
    }
}