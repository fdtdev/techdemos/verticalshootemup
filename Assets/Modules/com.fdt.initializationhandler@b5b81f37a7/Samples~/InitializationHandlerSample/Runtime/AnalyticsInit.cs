using System.Collections;
using UnityEngine;

namespace com.FDT.InitializationHandler.Sample
{
    /// <summary>
    /// Creation Date:   8/5/2020 8:40:37 PM
    /// Product Name:    FDT Initialization Handler
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:
    /// Changelog:       
    /// </summary>
    public class AnalyticsInit : InitWrapperItemBase
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Properties, Consts and Statics
        
        #endregion

        #region Variables
        
        #endregion

        #region Public API
        
        #endregion
                        
        #region Methods
        //[ContextMenu("call from context menu")]
        //public void Test() { }        
        #endregion

        
        public override void Init()
        {
            StartCoroutine(DoInit(5));
        }

        protected IEnumerator DoInit(float t)
        {
            yield return new WaitForSeconds(t);
            _initDependences.InitState = InitState.INITIALIZED;
            Debug.Log($"{this} finish");
        }
    }
}