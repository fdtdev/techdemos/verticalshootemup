﻿using System.Collections.Generic;
using com.FDT.Common.Editor;
using UnityEditor;
using UnityEngine;

namespace com.FDT.Sequences.Editor
{
    [CustomPropertyDrawer(typeof(SequenceItem))]
    public class SequenceItemDrawer : PropertyDrawer
    {
        #region Classes, Structs and Enums

        protected class ViewData
        {
            public SequenceItem item;
        }
        #endregion
        
        #region Variables
        protected Dictionary<string, ViewData> _viewDatas = new Dictionary<string, ViewData>();
        protected ViewData viewData;
        #endregion
        
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Color backgroundColor = GUI.backgroundColor;
            EditorGUI.BeginProperty(position, label, property);
            float sLine = EditorGUIUtility.singleLineHeight+4;
            GetViewData(property);

            GUI.Box(position, GUIContent.none, EditorStyles.helpBox);
            position.x += 1;
            position.y += 1;
            position.width -= 2;
            position.height -= 2;

            position.x += 8;
            position.width -= 8;
            float halfWidth = position.width / 2;

            float labelWidth = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = 60;
            // first rows
            Rect enabledRect = new Rect(position.x, position.y, halfWidth-10, sLine);
            Rect colorRect = new Rect(position.x+halfWidth, position.y, halfWidth, sLine);
            Rect asyncRect = new Rect(position.x, position.y + sLine, halfWidth-10, sLine);
            Rect realtimeRect = new Rect(position.x+halfWidth, position.y + sLine, halfWidth, sLine);
            SerializedProperty asyncProp = property.FindPropertyRelative("async");

            SerializedProperty enabledProp = property.FindPropertyRelative("enabled");
            EditorGUI.PropertyField(enabledRect, enabledProp);

            SerializedProperty colorProp = property.FindPropertyRelative("color");
            EditorGUI.BeginChangeCheck();
            EditorGUI.PropertyField(colorRect, colorProp);
            if (EditorGUI.EndChangeCheck())
            {
                Color c = colorProp.colorValue;
                c.a = 1f;
                colorProp.colorValue = c;
            }

            EditorGUI.PropertyField(asyncRect, asyncProp);
            EditorGUI.PropertyField(realtimeRect, property.FindPropertyRelative("realtime"));
            // description

            SerializedProperty descProp = property.FindPropertyRelative("description");
            float h = EditorGUI.GetPropertyHeight(descProp);
            Rect descRect = new Rect(position.x, position.y + (sLine * 2), position.width, h);
            EditorGUI.PropertyField(descRect, descProp);
            
            // waits
            EditorGUIUtility.labelWidth = 100;
            Rect waitBeforeRect = new Rect(position.x, position.y+(sLine*2)+h, halfWidth-10, sLine);
            Rect waitAfterRect = new Rect(position.x + halfWidth, position.y + (sLine * 2) + h, halfWidth, sLine);

            EditorGUI.PropertyField(waitBeforeRect, property.FindPropertyRelative("waitBefore"));
            EditorGUI.PropertyField(waitAfterRect, property.FindPropertyRelative("waitAfter"));

            // unity event
            SerializedProperty evtProp;
            if (asyncProp.boolValue)
            {
                evtProp = property.FindPropertyRelative("actionAsync");
            }
            else
            {
                evtProp = property.FindPropertyRelative("action");
            }
            float evtH = EditorGUI.GetPropertyHeight(evtProp);
            Rect evtRect = new Rect(position.x, position.y+(sLine*3)+h+10, position.width, evtH);
            EditorGUI.PropertyField(evtRect, evtProp);
            EditorGUIUtility.labelWidth = labelWidth;
            GUI.backgroundColor = backgroundColor;

            EditorGUI.EndProperty();
        }

        private void GetViewData(SerializedProperty property)
        {
            if (!_viewDatas.TryGetValue(property.propertyPath, out viewData))
            {
                viewData = new ViewData();
                _viewDatas[property.propertyPath] = viewData;
                viewData.item = ReflectionExtensions.GetTargetObjectOfProperty(property) as SequenceItem;
            }
        }
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            SerializedProperty descProp = property.FindPropertyRelative("description");
            float sLine = EditorGUIUtility.singleLineHeight+2;
            float h = EditorGUI.GetPropertyHeight(descProp);
            SerializedProperty evtProp;
            SerializedProperty asyncProp = property.FindPropertyRelative("async");
            if (asyncProp.boolValue)
            {
                evtProp = property.FindPropertyRelative("actionAsync");
            }
            else
            {
                evtProp = property.FindPropertyRelative("action");
            }
            float evtH = EditorGUI.GetPropertyHeight(evtProp);
            return h + evtH + (sLine * 3)+15;
        }
    }
}