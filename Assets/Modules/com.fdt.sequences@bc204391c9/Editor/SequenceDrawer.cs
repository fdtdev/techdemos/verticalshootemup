﻿using System.Collections.Generic;
using System.Linq;
using com.FDT.Common.Editor;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace com.FDT.Sequences.Editor
{
    [CustomPropertyDrawer(typeof(Sequence))]
    public class SequenceDrawer : PropertyDrawer
    {
        #region Classes, Structs and Enums

        protected class ViewData
        {
            public ReorderableList list;
            public SerializedProperty listProp;
            public float listHeight;
            public Sequence sequence;
            public List<SequenceItem> itemsList;
            public Texture2D selectedTexture;
            public Texture2D executingTexture;
            public Texture2D timeTexture;
        }
                
        #endregion
        #region Variables
        protected Dictionary<string, ViewData> _viewDatas = new Dictionary<string, ViewData>();
        protected ViewData viewData;
        protected float listItemHeight = 24;
        protected int lastSelected = -1;
        #endregion
        #region Methods

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            GUI.Box(position, GUIContent.none, EditorStyles.helpBox);
            position.x += 4;
            position.y += 4;
            position.width-=8;
            position.height-=8;
            
            GetViewData(property);

            float lHeight = 0;
            Rect listRect = new Rect(position.x, position.y, position.width, viewData.listHeight);
            viewData.list.draggable = viewData.list.displayAdd = viewData.list.displayRemove = !Application.isPlaying;
             
            viewData.list.DoList(listRect);
            lHeight = 0;
            int l = viewData.listProp.arraySize;
            for (int i = 0; i < l; i++)
            {
                lHeight += listItemHeight;
            }
            SerializedProperty onfinishEvtProp = property.FindPropertyRelative("_onFinishEvt");
            float evtH = EditorGUI.GetPropertyHeight(onfinishEvtProp);
            Rect evtRect = new Rect(position.x, listRect.y + listRect.height + 10, position.width, evtH);
            EditorGUI.PropertyField(evtRect, onfinishEvtProp);
            
            if (viewData.list.index != -1)
            {
                SerializedProperty item = viewData.listProp.GetArrayElementAtIndex(viewData.list.index);
                float itemHeight = EditorGUI.GetPropertyHeight(item);
                Rect labelRect = new Rect(position.x, evtRect.y + evtH /*+ 50*/, position.width, 20);
                GUI.Label(labelRect, "Selected Item:");
                Rect itemRect = new Rect(position.x, labelRect.y + 22, position.width, itemHeight);
                EditorGUI.PropertyField(itemRect, item, true);
            }

            
            EditorGUI.EndProperty();
        }

        private void GetViewData(SerializedProperty property)
        {
            if (!_viewDatas.TryGetValue(property.propertyPath, out viewData))
            {
                viewData = new ViewData();
                _viewDatas[property.propertyPath] = viewData;
                viewData.listProp = property.FindPropertyRelative( "_items" );
                GetLists( viewData, property );
                viewData.sequence = ReflectionExtensions.GetTargetObjectOfProperty(property) as Sequence;
                viewData.itemsList = ReflectionExtensions.GetTargetObjectOfProperty(viewData.listProp) as List<SequenceItem>;
                Color selectedColor = Color.cyan;
                selectedColor.a = 0.25f;
                
                var texture = new Texture2D(2, 2);
                Color[] pixels = Enumerable.Repeat(selectedColor, Screen.width * Screen.height).ToArray();
                texture.SetPixels(pixels);
                texture.Apply();
                viewData.selectedTexture = texture;
                selectedColor = Color.blue;
                selectedColor.a = 0.25f;
                texture = new Texture2D(2, 2);
                pixels = Enumerable.Repeat(selectedColor, Screen.width * Screen.height).ToArray();
                texture.SetPixels(pixels);
                texture.Apply();
                viewData.executingTexture = texture;
                
                selectedColor = Color.green;
                selectedColor.a = 0.75f;
                texture = new Texture2D(2, 2);
                pixels = Enumerable.Repeat(selectedColor, Screen.width * Screen.height).ToArray();
                texture.SetPixels(pixels);
                texture.Apply();
                viewData.timeTexture = texture;
            }
        }

        private void GetLists( ViewData viewData , SerializedProperty prop)
        {
            if (viewData.list == null)
            {
                viewData.list = GetList(viewData.listProp, "Items");
                viewData.listHeight = viewData.list.GetHeight();
            }
        }

        private ReorderableList GetList(SerializedProperty listProp, string header)
        {
            ReorderableList result = new ReorderableList( listProp.serializedObject, listProp );
                
            result.drawHeaderCallback += rect =>
            {
                EditorGUI.LabelField(rect, header);
            };
            result.drawElementCallback += (rect, index, active, focused) =>
            {
                var itemProp = listProp.GetArrayElementAtIndex(index);
                var item = viewData.itemsList[index];
                SerializedProperty colorProp = itemProp.FindPropertyRelative("color");
                Color bc = GUI.backgroundColor;
                if (Application.isPlaying && item.isCurrent)
                {
                    Rect r = new Rect(rect.x - 4, rect.y, rect.width + 8, rect.height);
                    if (item.isExecuting)
                    {
                        
                        GUI.DrawTexture(r, viewData.executingTexture);
                        GUI.backgroundColor = Color.blue;
                        GUI.Box(r, GUIContent.none, EditorStyles.helpBox);
                    }
                    else
                    {
                        GUI.DrawTexture(r, viewData.selectedTexture);
                        GUI.backgroundColor = Color.cyan;
                        GUI.Box(r, GUIContent.none, EditorStyles.helpBox);
                    }
                }
                else
                {
                    GUI.backgroundColor = colorProp.colorValue;
                    GUI.Box(rect, GUIContent.none, EditorStyles.helpBox);
                }
                GUI.backgroundColor = bc;
                Rect descRect = new Rect(rect.x + 20, rect.y+3, (rect.width * 0.5f)- 20, 18);
                EditorGUI.TextField(descRect, itemProp.FindPropertyRelative("description").stringValue);
                
                Rect beforeRect = new Rect(rect.x + (rect.width*0.5f)+10, rect.y+3, (rect.width * 0.25f)-15, 18);
                GUI.Box(beforeRect, GUIContent.none, EditorStyles.helpBox);
                Rect afterRect = new Rect(rect.x + (rect.width*0.75f)+5, rect.y+3, (rect.width * 0.25f)-15, 18);
                GUI.Box(afterRect, GUIContent.none, EditorStyles.helpBox);
                
                SerializedProperty waitBeforeProp = itemProp.FindPropertyRelative("waitBefore");
                SerializedProperty waitAfterProp = itemProp.FindPropertyRelative("waitAfter");
                GUI.Label(beforeRect, $"wait before:  {waitBeforeProp.floatValue}" );
                GUI.Label(afterRect, $"wait after:  {waitAfterProp.floatValue}" );

                if (Application.isPlaying && item.isCurrent)
                {
                    float aw = item.waitAfterProgress * afterRect.width;
                    float bw = item.waitBeforeProgress * beforeRect.width;
                
                    if (item.isWaitingBefore)
                    {
                        GUI.DrawTexture(new Rect(beforeRect.x, beforeRect.y, bw, beforeRect.height),
                            viewData.timeTexture);
                    }

                    if (item.isWaitingAfter)
                    {
                        GUI.DrawTexture(new Rect(afterRect.x, afterRect.y, aw, afterRect.height),
                            viewData.timeTexture);
                    }
                }
            };
            
            result.elementHeightCallback += index =>
            {
                return listItemHeight;
            };
            return result;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            GetViewData(property);
            int l = viewData.listProp.arraySize;
            float h = 70;
            for (int i = 0; i < l; i++)
            {
                h += listItemHeight;
            }

            if (viewData.list.index != -1)
            {
                SerializedProperty item = viewData.listProp.GetArrayElementAtIndex(viewData.list.index);
                float itemHeight = EditorGUI.GetPropertyHeight(item);
                h += itemHeight + 22;

            }
            SerializedProperty onfinishEvtProp = property.FindPropertyRelative("_onFinishEvt");
            float evtH = EditorGUI.GetPropertyHeight(onfinishEvtProp);
            h += evtH;
            return h;
        }
        #endregion
    }
}