﻿using UnityEngine;

namespace com.FDT.Sequences
{
    public class SequenceComponent : MonoBehaviour
    {
        public bool autoPlay = false;
        public Sequence sequence;

        void Start()
        {
            if (autoPlay)
                sequence.Run();
        }

        public void Run()
        {
            sequence.Run();
        }
        private void OnDestroy()
        {
            sequence.OnDestroy();
        }
    }
}