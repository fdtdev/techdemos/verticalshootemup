﻿using UnityEditor;
using UnityEngine;

namespace com.FDT.Common.Editor
{
    /// <summary>
    /// Creation Date:   27/02/2020 22:12:07
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    [InitializeOnLoad, DefaultExecutionOrder(-1000)]
    public static class RuntimeApplicationEditor
    {
        static RuntimeApplicationEditor()
        {
            EditorApplication.playModeStateChanged += LogPlayModeState;
        }
        private static void LogPlayModeState(PlayModeStateChange state)
        {
            if (state == PlayModeStateChange.ExitingEditMode)
            {
                RuntimeApplication.isQuittingEditor = false;
                RuntimeApplication.ApplicationisPlaying = true;
                RuntimeApplication.OnEnterPlayMode?.Invoke();
            }
            if (state == PlayModeStateChange.ExitingPlayMode)
            {
                RuntimeApplication.isQuittingEditor = true;
                RuntimeApplication.OnExitPlayMode?.Invoke();
            }
            if (state == PlayModeStateChange.EnteredEditMode)
            {
                RuntimeApplication.ApplicationisPlaying = false;
            }
        }
    }
}
