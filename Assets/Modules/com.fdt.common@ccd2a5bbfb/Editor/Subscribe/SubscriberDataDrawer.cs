using System.Collections;
using System.Collections.Generic;
using com.FDT.Common.Editor;
using UnityEditor;
using UnityEngine;

namespace com.FDT.Common.Subscribe.Editor
{
    /// <summary>
    /// Creation Date:   3/4/2021 10:25:24 PM
    /// Product Name:    FDTModules
    /// Developers:      franc
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [CustomPropertyDrawer(typeof(SubscriberData))]
    public class SubscriberDataDrawer : InstancedPropertyDrawer<SubscriberData, SubscriberDataDrawer.ViewData>
    {
        #region Classes, Structs and Enums

        public class ViewData : ViewDataBase
        {
            
        }
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Properties, Consts and Statics

        #endregion

        #region Variables
        
        #endregion

        #region Public API
        
        #endregion
                        
        #region Methods

        protected override void DoOnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            base.DoOnGUI(position, property, label);
            EditorGUI.BeginProperty(position, label, property);
            if (Application.isPlaying)
            {
                var all = viewData.cTarget.GetRegistered();
                int l = all.Count;
                float itemHeight = EditorGUIUtility.singleLineHeight + 2;
                Rect itemRect;
                float ch = 0;
                foreach (var item in all)
                {
                    itemRect = new Rect(position.x, position.y + ch, position.width, itemHeight);
                    EditorGUI.ObjectField(itemRect, item as Object, typeof(Object));
                    ch += itemHeight;
                }
            }
            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            GetViewData(property);
            if (Application.isPlaying)
            {
                var all = viewData.cTarget.GetRegistered();
                int l = all.Count;
                float itemHeight = EditorGUIUtility.singleLineHeight + 2;
                float ch = itemHeight * l;
                return ch;
            }
            return base.GetPropertyHeight(property, label);
        }

        #endregion
    }
}