﻿using UnityEditor;

namespace com.FDT.Common.Editor
{
    /// <summary>
    /// Creation Date:   23/02/2020 21:44:50
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    [CustomEditor(typeof(ScriptableObjectWithDescription), true)]
    public class ScriptableObjectWithDescriptionEditor : UnityEditor.Editor
    {
        #region Methods
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            DrawDefaultInspector();
            serializedObject.ApplyModifiedProperties();
        }
        #endregion
    }
}