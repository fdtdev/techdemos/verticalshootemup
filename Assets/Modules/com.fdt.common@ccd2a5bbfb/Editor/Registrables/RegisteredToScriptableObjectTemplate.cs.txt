using com.FDT.Common.Registrables;

namespace #NAMESPACE#
{
    // --------------
    /// Creation Date:   #CREATIONDATE#
    /// Product Name:    #PROJECTNAME#
    /// Developers:      #DEV#
    /// Company:         #COMPANY#
    // Description:     #NOTRIM#
    // --------------
    public class #SCRIPTNAME# : RegisteredToScriptableObject<#Registerer#, #SCRIPTNAME#, #RegistrableID#>
    {
        #region Classes and Structs
        #NOTRIM#
        #endregion

        #region GameEvents and UnityEvents
        //[Header("GameEvents"), SerializeField] 
        //[Header("UnityEvents"), SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        #NOTRIM#
        #endregion

        #region Inspector Fields
        #NOTRIM#
        #endregion

        #region Properties, Consts and Statics
        protected override bool RegisterOnEnable => #REGISTERONENABLE#;
        #endregion

        #region Methods
        #NOTRIM#
        #endregion
    }
}