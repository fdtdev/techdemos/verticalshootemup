namespace com.FDT.Common.Editor
{
    /// <summary>
    /// Creation Date:   23/02/2020 20:52:50
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public enum FailReason
    {
        NONE = 0,
        UNKNOWN = 1,
        DOUBLE_DOT = 2,
        SPACE = 3,
        ENDSWITH_DOT = 4,
        STARTSWITH_DOT = 5,
        CONTAINS_RESERVED = 6
    }
}
