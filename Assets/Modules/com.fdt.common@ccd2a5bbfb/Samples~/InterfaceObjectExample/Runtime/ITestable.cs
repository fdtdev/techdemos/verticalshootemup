namespace com.FDT.Common.Example
{
    /// <summary>
    /// Creation Date:   10/4/2020 1:43:56 PM
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public interface ITestable
    {
        int a { get; }
    }
}