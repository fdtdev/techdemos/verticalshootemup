using UnityEngine;

namespace com.FDT.Common.Example
{
    /// <summary>
    /// Creation Date:   10/4/2020 1:44:26 PM
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public class Testable : MonoBehaviour, ITestable
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Properties, Consts and Statics
        
        #endregion

        #region Variables
        
        #endregion

        #region Public API
        
        #endregion
                        
        #region Methods
        //[ContextMenu("call from context menu")]
        //public void Test() { }        
        #endregion

        [SerializeField] protected int _a;
        public int a => _a;
    }
}