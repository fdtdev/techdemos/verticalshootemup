using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.Common
{
    /// <summary>
    /// Creation Date:   7/14/2020 11:13:09 PM
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public class ObjectTypeTest : MonoBehaviour
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("GameEvents"), SerializeField] 
        //[Header("UnityEvents"), SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("Public"), SerializeField] 
        //[Header("Protected"), SerializeField]
        //[Header("Private"), SerializeField]
        [SerializeField, ObjectType(typeof(IObjectType))] protected List<Object> testObjects = new List<Object>();
        #endregion

        #region Properties, Consts and Statics
        
        #endregion

        #region Variables
        
        #endregion

        #region Public API
        #endregion
                        
        #region Methods

        private void OnEnable()
        {
            foreach (var o in testObjects)
            {
                if (o is IObjectType i)
                {
                    Debug.Log($"{i} is ok");
                }
                else
                {
                    Debug.Log($"{o} is NOT ok");
                }
            }
        }

        #endregion
    }
}