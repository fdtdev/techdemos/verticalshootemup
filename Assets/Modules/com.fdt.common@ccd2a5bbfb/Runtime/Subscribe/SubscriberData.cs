using System.Collections.Generic;

namespace com.FDT.Common.Subscribe
{
    /// <summary>
    /// Creation Date:   3/4/2021 10:21:57 PM
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// Changelog:         
    /// </summary>
    [System.Serializable]
    public class SubscriberData
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("")]
        protected HashSet<ISubscribable> _subscribed = new HashSet<ISubscribable>(); 
        #endregion

        #region Properties, Consts and Statics
        
        #endregion

        #region Variables
        
        #endregion

        #region Public API
        
        #endregion
                        
        #region Methods
        //[ContextMenu("call from context menu")]
        //public void Test() { }        
        #endregion

        public bool Register(ISubscribable item)
        {
            if (!_subscribed.Contains(item))
            {
                _subscribed.Add(item);
                return true;
            }
            return false;
        }

        public bool Unregister(ISubscribable item)
        {
            if (_subscribed.Contains(item))
            {
                _subscribed.Remove(item);
                return true;
            }
            return false;
        }

        public HashSet<ISubscribable> GetRegistered()
        {
            return _subscribed;
        }
    }
}