﻿using System;
using UnityEngine;

namespace com.FDT.Common
{
	/// <summary>
	/// Creation Date:   13/05/2020 15:22:35
	/// Product Name:    FDT Common
	/// Developers:      Jon Kenkel (nonathaj), FDT Dev 
	/// Company:         FDT Dev
	/// Description:	 Singleton Class.
	/// 				 As a note, this is made as MonoBehaviour because we need Coroutines.
	/// 				 original version from https://wiki.unity3d.com/index.php/Secure_UnitySingleton
	/// Changelog:		 --/--/-- - Modified to allow the usage without the use of the singleton attribute.
	/// 				 --/--/-- - Fixed problem when a singleton instance exists in a scene, but nobody used
	///					   		    it's instance yet, and something checks if it exists by using the method Exists
	/// </summary>
	public abstract class Singleton<T> : MonoBehaviour, ISingleton where T : MonoBehaviour, ISingleton
	{
		private static T _instance;

		private static object _lock = new object();

		public static bool Exists
		{
			get { return _instance != null; }
		}

		protected void Awake()
		{
			if (Exists && _instance != null && _instance != this)
				Destroy(gameObject);
			if (_instance == null)
				_instance = this as T;
			OnAwake();
		}

		protected virtual void OnAwake()
		{

		}

		/// <summary>
		/// Destroy the current static instance of this singleton
		/// </summary>
		/// <param name="destroyGameObject">Should we destroy the gameobject of the instance too?</param>
		public static void DestroyInstance(bool destroyGameObject = true)
		{
			if (Exists)
			{
				if (destroyGameObject)
					Destroy(_instance.gameObject);
				else
					Destroy(_instance);
				_instance = null;
			}
		}

		/// <summary>
		/// Ensures that an instance of this singleton is generated
		/// </summary>
		public static void TouchInstance()
		{
			if (!Exists && !applicationIsQuitting)
				Generate();
		}

		/// <summary>
		/// Generates this singleton
		/// </summary>
		private static void Generate()
		{
			UnitySingletonAttribute attribute =
				Attribute.GetCustomAttribute(typeof(T), typeof(UnitySingletonAttribute)) as UnitySingletonAttribute;
			if (attribute == null)
			{
				TryGenerateInstance(UnitySingletonAttribute.Type.CreateOnNewGameObject, true, String.Empty, false);
				//Debug.LogError("Cannot find UnitySingleton attribute on " + typeof(T).Name);
				return;
			}

			for (int x = 0; x < attribute.singletonTypePriority.Length; x++)
			{
				if (TryGenerateInstance(attribute.singletonTypePriority[x], attribute.destroyOnLoad,
					attribute.resourcesLoadPath, x == attribute.singletonTypePriority.Length - 1))
					break;
			}
		}

		/// <summary>
		/// Returns an instance of this singleton (if it does not exist, generates one based on T's UnitySingleton Attribute settings)
		/// </summary>
		public static T Instance
		{
			get
			{
				TouchInstance();
				return _instance;
			}
			set
			{
				UnitySingletonAttribute attribute =
					Attribute.GetCustomAttribute(typeof(T), typeof(UnitySingletonAttribute)) as UnitySingletonAttribute;
				if (attribute == null)
					Debug.LogError("Cannot find UnitySingleton attribute on " + typeof(T).Name);

				if (attribute.allowSetInstance)
					_instance = value;
				else
					Debug.LogError($"{typeof(T).Name} is not allowed to set instances.  Please set the allowSetInstace flag to true to enable this feature.");
			}
		}

		public virtual void OnDynamicCreation()
		{

		}

		private static bool applicationIsQuitting = false;

		/// <summary>
		/// When Unity quits, it destroys objects in a random order.
		/// In principle, a Singleton is only destroyed when application quits.
		/// If any script calls Instance after it have been destroyed, 
		///   it will create a buggy ghost object that will stay on the Editor scene
		///   even after stopping playing the Application. Really bad!
		/// So, this was made to be sure we're not creating that buggy ghost object.
		/// </summary>
		public void OnDestroy()
		{
			applicationIsQuitting = true;
		}

		/// <summary>
		/// Attempts to generate a singleton with the given parameters
		/// </summary>
		/// <param name="type"></param>
		/// <param name="resourcesLoadPath"></param>
		/// <param name="warn"></param>
		/// <returns></returns>
		private static bool TryGenerateInstance(UnitySingletonAttribute.Type type, bool destroyOnLoad,
			string resourcesLoadPath, bool warn)
		{
			if (type == UnitySingletonAttribute.Type.ExistsInScene)
			{
				_instance = GameObject.FindObjectOfType<T>();
				if (_instance == null)
				{
					if (warn)
						Debug.LogError($"Cannot find an object with a {typeof(T).Name}.  Please add one to the scene.");
					return false;
				}
			}
			else if (type == UnitySingletonAttribute.Type.LoadedFromResources)
			{
				if (string.IsNullOrEmpty(resourcesLoadPath))
				{
					if (warn)
						Debug.LogError(
							$"UnitySingletonAttribute.resourcesLoadPath is not a valid Resources location in {typeof(T).Name}");
					return false;
				}

				T pref = Resources.Load<T>(resourcesLoadPath);
				if (pref == null)
				{
					if (warn)
						Debug.LogError($"Failed to load prefab with {typeof(T).Name} component attached to it from folder Resources/{resourcesLoadPath}.  Please add a prefab with the component to that location, or update the location.");
					return false;
				}

				_instance = Instantiate<T>(pref);
				if (_instance == null)
				{
					if (warn)
						Debug.LogError($"Failed to create instance of prefab {pref} with component {typeof(T).Name}.  Please check your memory constraints");
					return false;
				}
			}
			else if (type == UnitySingletonAttribute.Type.CreateOnNewGameObject)
			{
				GameObject go = new GameObject(typeof(T).Name + " Singleton");
				if (go == null)
				{
					if (warn)
						Debug.LogError($"Failed to create gameobject for instance of {typeof(T).Name}.  Please check your memory constraints.");
					return false;
				}

				_instance = go.AddComponent<T>();
				if (_instance == null)
				{
					if (warn)
						Debug.LogError($"Failed to add component of {typeof(T).Name} to new gameobject.  Please check your memory constraints.");
					Destroy(go);
					return false;
				}
			}

			if (!destroyOnLoad)
				DontDestroyOnLoad(_instance.gameObject);

			return true;
		}
	}
}