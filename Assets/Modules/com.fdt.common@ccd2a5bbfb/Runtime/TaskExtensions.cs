using System.Threading;
using System.Threading.Tasks;

namespace com.FDT.Common
{
    /// <summary>
    /// Creation Date:   2/22/2021 10:06:26 PM
    /// Product Name:    FDT Initialization Handler
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:
    /// Changelog:        
    /// </summary>
    public static class TaskExtensions
    {
        /*
        public async Task<Results> ProcessDataAsync(MyData data, CancellationToken token)
        {
            Client client;
            try
            {
                client = await GetClientAsync().WaitOrCancel(token);
                await client.UploadDataAsync(data).WaitOrCancel(token);
                await client.CalculateAsync().WaitOrCancel(token);
                return await client.GetResultsAsync().WaitOrCancel(token);
            }
            catch (OperationCanceledException)
            {
                if (client != null)
                    await client.CancelAsync();
                throw;
            }
        }
         */
        public static async Task<T> WaitOrCancel<T>(this Task<T> task, CancellationToken token)
        {
            token.ThrowIfCancellationRequested();
            await Task.WhenAny(task, token.WhenCanceled());
            token.ThrowIfCancellationRequested();

            return await task;
        }
        public static Task WhenCanceled(this CancellationToken cancellationToken)
        {
            var tcs = new TaskCompletionSource<bool>();
            cancellationToken.Register(s => ((TaskCompletionSource<bool>)s).SetResult(true), tcs);
            return tcs.Task;
        }
    }
}