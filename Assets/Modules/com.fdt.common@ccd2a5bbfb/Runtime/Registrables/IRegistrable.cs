﻿namespace com.FDT.Common.Registrables
{
    /// <summary>
    /// Creation Date:   28/02/2020 15:46:31
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public interface IRegistrable<TRegistrableID>:IRegistrableBase where TRegistrableID : IRegistrableID
    {
        #region Public API
        TRegistrableID referenceID { get; }
        #endregion
    }
}