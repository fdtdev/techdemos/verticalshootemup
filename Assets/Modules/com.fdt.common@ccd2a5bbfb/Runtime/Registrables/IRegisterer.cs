﻿namespace com.FDT.Common.Registrables
{
    /// <summary>
    /// Creation Date:   28/02/2020 15:47:10
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public interface IRegisterer<TRegistrable, TRegistrableID>:IRegistererBase where TRegistrable : IRegistrable<TRegistrableID>
        where TRegistrableID : IRegistrableID
    {
        #region Public API
        bool Register(TRegistrable registeredItem);
        bool Unregister(TRegistrable registeredItem);
        #endregion
    }
}