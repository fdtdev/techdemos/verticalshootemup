using System;
using UnityEngine;

namespace com.FDT.Common
{
    /// <summary>
    /// Creation Date:   10/4/2020 1:27:19 PM
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    [System.Serializable]
    public class InterfaceObject<T>:InterfaceObjectBase where T : class
    {
        #region Inspector Fields
        [SerializeField] protected UnityEngine.Object _data;
        protected T data;
        #endregion

        #region Properties, Consts and Statics
        public override Type FilterType
        {
            get => typeof(T);
        }
        public T Value
        {
            get
            {
                if (_changed || data == null)
                {
                    data = _data as T;
                    _changed = false;
                }
                return data;
            }
            set
            {
                data = value;
                _data = data as UnityEngine.Object;
                _changed = false;
            }

        }

        #endregion
    }
}