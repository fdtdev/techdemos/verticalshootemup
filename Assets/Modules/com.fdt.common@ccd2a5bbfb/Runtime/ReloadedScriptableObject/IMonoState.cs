namespace com.FDT.Common.ReloadedScriptableObject
{
    /// <summary>
    /// Creation Date:   3/2/2021 11:15:16 PM
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     MonoState main interface. Its implemented by ScriptableObjects that hold static data,
    ///                  to be properly reset between play sessions in editor.
    /// Changelog:         
    /// </summary>
    public interface IMonoState
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Properties, Consts and Statics
        
        #endregion

        #region Variables
        
        #endregion

        #region Public API
        
        #endregion
                        
        #region Methods
        void ResetValues();      
        #endregion
    }
}