using UnityEngine;

namespace com.FDT.Pools.BasicLoadedResources
{
    /// <summary>
    /// Creation Date:   2/26/2021 10:12:08 PM
    /// Product Name:    FDT Pools
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:
    /// Changelog:            
    /// </summary>
    public class PooleableItem : MonoBehaviour, IPooleable
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Properties, Consts and Statics
        
        #endregion

        #region Variables
        
        #endregion

        #region Public API
        
        #endregion
                        
        #region Methods
        //[ContextMenu("call from context menu")]
        //public void Test() { }        
        #endregion

        protected IPool _pool;
        [SerializeField] private PoolData _poolData;
        public PoolData poolData { get => _poolData; }

        public void ReturnToPool()
        {
            poolData.ReturnToPool();
        }
    }
}