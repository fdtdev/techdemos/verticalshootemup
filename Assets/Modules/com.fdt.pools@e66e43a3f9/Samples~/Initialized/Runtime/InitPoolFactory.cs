using com.FDT.InitializationHandler;
using UnityEngine;

namespace com.FDT.Pools.Initialized
{
    /// <summary>
    /// Creation Date:   2/26/2021 10:19:45 PM
    /// Product Name:    FDT Pools
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:
    /// Changelog:            
    /// </summary>
    public class InitPoolFactory<T, T2> : PoolFactory<T, T2>, IInitWrapper where T : Object, IPooleable where T2 : IPoolAssetProvider
    {
        public void Init()
        {
            Load(() => { _initHandlerData.InitState = InitState.INITIALIZED; });
        }

        [SerializeField] protected InitHandlerData _initHandlerData;
        public InitHandlerData InitData { get =>_initHandlerData; }
        public bool IsEnabled { get => true; }
    }
}