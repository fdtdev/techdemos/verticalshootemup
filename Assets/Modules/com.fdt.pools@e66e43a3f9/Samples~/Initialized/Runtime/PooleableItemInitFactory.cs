namespace com.FDT.Pools.Initialized
{
    /// <summary>
    /// Creation Date:   2/26/2021 10:12:55 PM
    /// Product Name:    FDT Pools
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:
    /// Changelog:            
    /// </summary>
    public class PooleableItemInitFactory : InitPoolFactory<PooleableItem, PoolResourcesProvider>
    {

    }
}