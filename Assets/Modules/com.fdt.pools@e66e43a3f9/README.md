# FDT Pools

Generic pool system with multiple possible sources


## Usage

The package must be included using the manifest.json file in the project.
The file must be modified to include this dependencies:

```json
{
  "dependencies": {
	"com.fdt.pools": "https://gitlab.com/fdtmodules/fdtpools.git#2021.1.0",
	"com.fdt.common": "https://gitlab.com/fdtmodules/fdtcommon.git#2021.1.0",

	...
  }
}

```
## Useful links

[Documentation](https://gitlab.com/fdtmodules/fdtpools/-/wikis/home)

[FDT Modules](https://gitlab.com/fdtmodules)
## License

MIT - see [LICENSE](https://gitlab.com/fdtmodules/fdtpools/src/2021.1.0/LICENSE.md)
