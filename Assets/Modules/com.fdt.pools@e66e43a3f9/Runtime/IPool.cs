﻿using System;
using System.Threading.Tasks;

namespace com.FDT.Pools
{
    /// <summary>
    /// Product Name:    FDT Pools
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     Interface implemented by pools.
    /// Changelog:       
    /// </summary>
    public interface IPool
    {
        void Return(IPooleable gameActor);
        IPooleable Get(bool b);
        Task Load(Action callback);
    }
}