using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using com.FDT.Common;
using UnityEngine;

namespace com.FDT.Pools
{
    /// <summary>
    /// Creation Date:   2/27/2021 12:07:55 AM
    /// Product Name:    FDTModules
    /// Developers:      FDT
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [System.Serializable]
    public class PoolResourcesScriptableProvider : IPoolAssetProvider
    {
        [SerializeField] protected IDAsset _prefabReference;
        public async Task<GameObject> GetAsset(CancellationToken t)
        {
            GameObject result;
            var prefabHandle = Resources.LoadAsync<GameObject>(_prefabReference.name);
            do
            {
                await Task.Yield();
            } while (!prefabHandle.isDone && !t.IsCancellationRequested);

            if (t.IsCancellationRequested)
                return null;
            
            return prefabHandle.asset as GameObject;
        }

        public void OnDestroy()
        {
            
        }
    }
}