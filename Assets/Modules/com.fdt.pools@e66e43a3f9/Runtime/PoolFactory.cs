using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace com.FDT.Pools
{
    /// <summary>
    /// Product Name:    FDT Pools
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     Abstract pool class for one kind of prefab object. It can preallocate objects. Can be set up to grow when needed.
    ///                  It initializes asynchronously, loading it's prefab. 
    /// Changelog:       
    /// </summary>
    public abstract class PoolFactory<T, T2> : MonoBehaviour, IPool where T : UnityEngine.Object, IPooleable where T2:IPoolAssetProvider
    {
        [SerializeField] protected T2 _assetProvider;   
        [SerializeField] protected int _preAllocate = 0;
        [SerializeField] protected bool _canGrow = false;
        [SerializeField] protected Transform _parent;
        [SerializeField] protected List<T> _pool = new List<T>();
        [SerializeField] private bool _autoInit = false;

        private CancellationTokenSource _tokenSource;
        private T prefab;
        private bool initialized = false;
        private void Start()
        {
            if (_autoInit)
            {
                Load();
            }
        }

        public virtual async Task Load(Action callback = null)
        {
            if (initialized)
            {
                Debug.Log($"pool {gameObject.name} already initialized.");
                callback?.Invoke();
                return;
            }

            initialized = true;
            if (_pool.Count > 0)
            {
                Debug.LogError($"Error in PoolFactory {gameObject.name} - There shouldn't be any object inside the pool before Init.");
            }

            _tokenSource = new CancellationTokenSource();
            var t = await _assetProvider.GetAsset(_tokenSource.Token);
            
            if (_tokenSource.IsCancellationRequested)
                return;
            
            prefab = t.GetComponent<T>();;
            prefab.gameObject.SetActive(false);
            if (_preAllocate > 0)
            {
                for (int i = 0; i < _preAllocate; i++)
                {
                    var o = CreatePoolObject();
                    _pool.Add(o);
                }
            }
            
            if (_tokenSource.IsCancellationRequested)
                return;

            Debug.Log($"pool {gameObject.name} completed initialization.");
            callback?.Invoke();
        }

        public IPooleable Get(bool activate = true)
        {
            for (int i = 0; i < _pool.Count; i++)
            {
                if (!_pool[i].isActiveAndEnabled)
                {
                    if (activate)
                        _pool[i].gameObject.SetActive(true);

                    return _pool[i];
                }
            }
            if (_canGrow)
            {
                var o = CreatePoolObject();
                _pool.Add(o);
                if (activate)
                    o.gameObject.SetActive(true);
                return o;
            }

            return null;
        }

        protected T CreatePoolObject() 
        {
            T result = Instantiate(prefab, _parent);
            result.poolData.SetData(this, result);
            return result;
        }

        public void Return(IPooleable o)
        {
            o.gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            _tokenSource?.Cancel();
            _assetProvider.OnDestroy();
        }
    }
}