using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace com.FDT.Pools
{
    /// <summary>
    /// Creation Date:   2/27/2021 12:08:11 AM
    /// Product Name:    FDTModules
    /// Developers:      FDT
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    public interface IPoolAssetProvider
    {
       Task<GameObject> GetAsset(CancellationToken t);

       void OnDestroy();
    }
}