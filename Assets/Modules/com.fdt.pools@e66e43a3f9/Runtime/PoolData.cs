namespace com.FDT.Pools
{
    /// <summary>
    /// Creation Date:   2/26/2021 10:30:30 PM
    /// Product Name:    FDT Pools
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:
    /// Changelog:            
    /// </summary>
    public class PoolData
    {
        private IPool pool;
        private IPooleable poolObj;
        public void ReturnToPool()
        {
            pool.Return(poolObj);
        }

        public void SetData(IPool p, IPooleable o)
        {
            pool = p;
            poolObj = o;
        }
    }
}